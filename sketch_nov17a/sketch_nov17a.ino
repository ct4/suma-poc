#include <AzureIoTHub.h>
#include "AzureIoTProtocol_MQTT.h"
#include "iothubtransportmqtt.h"
#include "config.h"
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

const char *ssid = APSSID;
const char *password = APPSK;

const int analogInPin = A0;  // ESP8266 Analog Pin ADC0 = A0

int sensor_value = 0;  // value read from the pot

// IoT stuff
#define USE_BALTIMORE_CERT
IOTHUB_DEVICE_CLIENT_LL_HANDLE device_ll_handle;
IOTHUB_MESSAGE_HANDLE message_handle;
IOTHUB_CLIENT_RESULT result;
static const char* connectionString = DEVICE_CONNECTION_STRING;

static void connection_status_callback(IOTHUB_CLIENT_CONNECTION_STATUS result, IOTHUB_CLIENT_CONNECTION_STATUS_REASON reason, void* user_context)
{
  // This sample DOES NOT take into consideration network outages.
  if (result == IOTHUB_CLIENT_CONNECTION_AUTHENTICATED)
    Serial.println("Connection status: The device client is connected to iothub\r\n");
  else {
    Serial.println(IOTHUB_CLIENT_CONNECTION_STATUS_REASONStrings(reason));
  }
}

static void send_confirm_callback(IOTHUB_CLIENT_CONFIRMATION_RESULT result, void* userContextCallback)
{
    (void)userContextCallback;
    Serial.print("Confirmation callback received for message %lu with result ");
    Serial.println(IOTHUB_CLIENT_CONFIRMATION_RESULTStrings(result));
}

void setup_wifi() {
  delay(1000);
  Serial.print("Configuring access point...");
  WiFi.begin(ssid, password);

  int i = 0;
  while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }

  IPAddress myIP = WiFi.localIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
}

void setup_time()
{
    time_t epochTime;
    configTime(0, 0, "pool.ntp.org", "time.nist.gov");

    while (true)
    {
        epochTime = time(NULL);

        if (epochTime == 0)
        {
            Serial.println("Fetching NTP epoch time failed! Waiting 2 seconds to retry.");
            delay(2000);
        }
        else
        {
            Serial.printf("Fetched NTP epoch time is: %lu.\r\n", epochTime);
            break;
        }
    }
}

void setup_iothub() {
  IOTHUB_CLIENT_TRANSPORT_PROVIDER protocol = MQTT_Protocol;

  (void)IoTHub_Init();
  device_ll_handle = IoTHubDeviceClient_LL_CreateFromConnectionString(connectionString, protocol);
  Serial.print("Creating IoTHub Device handle\r\n");

  if (device_ll_handle == NULL)
  {
      Serial.print("Error AZ002: Failure creating Iothub device. Hint: Check you connection string.\r\n");
  }
  else
  {
    // Setting the Trusted Certificate.
    IoTHubDeviceClient_LL_SetOption(device_ll_handle, OPTION_TRUSTED_CERT, certificates);

    bool urlEncodeOn = true;
    IoTHubDeviceClient_LL_SetOption(device_ll_handle, OPTION_AUTO_URL_ENCODE_DECODE, &urlEncodeOn);

    // Setting connection status callback to get indication of connection to iothub
    (void)IoTHubDeviceClient_LL_SetConnectionStatusCallback(device_ll_handle, connection_status_callback, NULL);
    IoTHubDeviceClient_LL_DoWork(device_ll_handle);
    Serial.println("set connection status callback");
  }
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  setup_time();
  setup_iothub();
}

void loop() {
  sensor_value = analogRead(analogInPin);
  Serial.println(sensor_value);
  IoTHubDeviceClient_LL_DoWork(device_ll_handle);

  char buf[100];
  buf[99] = '\0';
  sprintf(buf, "{ \"adcValue\": %i }", sensor_value);
  IoTHubDeviceClient_LL_DoWork(device_ll_handle);
  
  message_handle = IoTHubMessage_CreateFromString(buf);
  IoTHubDeviceClient_LL_DoWork(device_ll_handle);

  Serial.print("Sending message to IoTHub\r\n");
  result = IoTHubDeviceClient_LL_SendEventAsync(device_ll_handle, message_handle, send_confirm_callback, NULL);
  if(result != IOTHUB_CLIENT_OK)
    Serial.println("Failed to queue message to IOT hub");
  IoTHubDeviceClient_LL_DoWork(device_ll_handle);
  IoTHubMessage_Destroy(message_handle);
  IoTHubDeviceClient_LL_DoWork(device_ll_handle);
  delay(5000);
}
