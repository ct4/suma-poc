# Chirpstack Config files

## Web-UI based actions
  * Ensure to enable `Add gateway meta-data` checkbox in Service-profiles to allow concatination of RSSI and SQNR RX values from the gateway
  ![Add gateway meta-data](service-profile.png)